
// [ CREATING A SERVER ]
// 1. imported http thru require directive
// 2. we use the createServer()
// 3. define the port number that the server will be listening
// 4. use the listen() for the server to run in a specific port
// 5. console log to monitor the server

// Create a condition and a response when the route is accessed



	let http = require("http");
	const port = 4000;

	http.createServer(function(request,response){

	// HTTP Routing Methods: Get, Post, Put, Delete
		if(request.url == "/items" && request.method == "GET"){

		// HTTP method of the incoming request can be accessed via the method property of the request parameter

		// The method "Get" means that we will be retrieving/reading an info
			response.writeHead(200,{'Content-Type': 'text/plain'});
			response.end("Data retrieved from the database");
		}

		else if (request.url == "/items" && request.method == "POST"){
			
		// Requests the '/items' path and 'SENDS' info
			response.writeHead(200,{"Content-Type":"text/plain"});
			response.end("Data to be sent to the database");
		}

	}).listen(4000);

	console.log('Server running at localhost: 4000')