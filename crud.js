let http = require("http")

// Mock database
		let directory=[
			{
				"name" : "Brandon",
				"email" : "brandon@mail.com"
			},
			{
				"name" : "Jobert",
				"email" : "jobert@mail.com"
			}
		]


		const server = http.createServer(function(request,response) {

			// Route for returning all items upon receiving a GET request
			
			if (request.url == "/users" && request.method == "GET"){

				response.writeHead(200,{'Content-Type': 'application/json'})

			// Input has to be data type STRING, hence the JSON.stringify()
			// When sending data to a web server, the data has to be a string
				
				response.write(JSON.stringify(directory));
				response.end();
			}

			// add user to the database
			if (request.url == "/users" && request.method == "POST"){

				//Declare and initialize a 'requestBody' var to an empty string; this will act as a placeholder for the data to be created later on
				
				let requestBody = "";

				// A stream is a sequence of data; DAta is received from the client and is processed in the 'data stream'

				request.on('data', function(data){
				// Assigns the data retrieved from the data stream to the requestBody

				requestBody +=data;

				});

				// response end step - only runs after the request has completely been sent

				request.on('end', function(){

					console.log(typeof requestBody);

					requestBody = JSON.parse(requestBody);

					let newUser = {
						"name" : requestBody.name,
						"email" : requestBody.email
					}

					// Add new user into the mock db
					directory.push(newUser);
					console.log(directory);

					response.writeHead(200,{'Content-Type' : 'application/json'});
					response.write(JSON.stringify(newUser));
					response.end();
				});
			}

		}).listen(4000)

		console.log("Server running at localhost 4000");